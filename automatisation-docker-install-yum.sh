#!bin/bash

sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

#ajoute le repository de Docker CE à yum
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

#installe docker CE sur Centos
yum install -y docker-ce docker-ce-cli containerd.io

#démarre le service docker
systemctl start docker

#execute l'image hello-world pour tester que Docker fonctionne
docker run hello-world

#ajout de l'utilisateur courant au groupe docker
usermod -aG docker playercentos

#télécharge  docker-compose sur Centos
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

#ajoute les droits d'execution à docker-compose
chmod +x /usr/local/bin/docker-compose

#précise la version de docker-compose
docker-compose --version

#installe apache via le paquet httpd
yum install -y httpd

#active au démarrage le service httpd
systemctl enable httpd

#démarre le service httpd
systemctl start httpd

#ouvre le port 80 sur le firewall CentOs
firewall-cmd --zone=public --add-port=80/tcp --permanent

#autorise l'utilisateur à accéder à www-data
sudo chown -R www-data:www-data /var/www


