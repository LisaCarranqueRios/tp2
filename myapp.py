# -*- coding: utf-8 -*-

from flask import Flask, request, render_template
import redis
import time

app = Flask(__name__)

#connexion redis via python
r = redis.Redis(host='redis',port=6379)

@app.route("/")
def template_render():
    valueList = [] 
    for key in r.scan_iter():
         valueList.append(r.get(key).decode('utf-8'))
    return render_template('home.html', my_list=valueList)

@app.route("/valeur", methods=['GET','POST']) 
def esp():
   if request.method == 'POST': 
   
      distance = request.form["valeur"]
      ts = time.time()
      r.set(ts,distance) 
      return distance

   elif request.method == 'GET':
      retour = ''
      for key in r.scan_iter():
         retour += ("clef : " + str(key) + " - valeur : " + str(r.get(key)) + " || ")
      return str(retour)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

