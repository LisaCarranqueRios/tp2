FROM python:3
MAINTAINER srevereault

WORKDIR /app
RUN mkdir templates
COPY templates/home.html /app/templates/

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY myapp.py /app/

ENTRYPOINT ["python", "/app/myapp.py"]

